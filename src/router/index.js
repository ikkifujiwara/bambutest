import Vue from 'vue'
import Router from 'vue-router'
import index from '@/components/StarWars/index'
import show from '@/components/StarWars/show'

Vue.use(Router)

let routes = [
  {
    path: '/:page?',
    name: 'index',
    component: index
  },
  {
    path: '/show/:id',
    name: 'show',
    component: show
  }
]

const router = new Router({
  routes
})

export default router
