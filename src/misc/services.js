import axios from 'axios'
import { url } from './constants'

export const index = (page = 1) => {
  return axios.get(url + '?page=' + page)
}

export const show = (id) => {
  return axios.get(url + '/' + id)
}
