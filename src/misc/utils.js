import { url } from './constants'

export const replaceLink = (link) => {
  return link.replace(url, '')
}
